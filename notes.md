Scope:

Testen in het algemeen

Layout, visueel, CSS, cross-browser.


UX testing

Extra: testen van code. Maar wel belangrijk: de browser tools goed laten zien en belang benadrukken.

Een aantal principes van Testen:

* user-centered testing. Jesse James Garret benadrukt met veel voorbeelden hoeveel een paar tests met echte gebruikers waard zijn. Testen helpt om objectiever te worden.

Dit is leuk: Apple User Interface Design handbook, especially pp 9-19.https://www.apple2scans.net/files/1982-A2F2116-m-a2e-aiiedg.pdf

Nielsen 1994.

* test-driven development:
* release early, release often. (In jullie geval: naar github-pages). Het is  embarrassing als je een probleempje krijgt die je dan ook nog zelf moet snappen omdat je het nog nooit eerder heb gezien. Als je dit in een vroeg stadium naar boven haalt, ben je gedwongen om je applicatieomgeving beter te kennen.

Voorbeeld van een recent project waar ik dat niet deed: utrechtinvogelvlucht.nl. Heb niet naar een productie-like omgeving gepushed tot later in het project, waardoor:

* SSL testen nog niet kon (verschil in behandelen beveiliging tussen localhost en remote server met domeinnaam)
* nog nooit een versie had getest die was gebouwd met environment=production vs environment=development, dus een aantal dingen gingen mis.
* Aantal Windows/IE dingen niet snel gevonden

Maar let wel: het kan heel ontmoedigend en overweldigend zijn als er zoveel verschillende dingen op je af komen. Er is altijd een cost/benefit afweging met testen, vind ik.




Mogelijke opdrachten
--------------------

* debug een applicatie die niet werkt (eigen, of een voorbeeld dat ik geef)
* geef wat informatie over performance van een aantal sites: wat zijn de bottlenecks?
